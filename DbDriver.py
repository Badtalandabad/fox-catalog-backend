import mariadb


class DbDriver:
    db = None
    error = None

    user = 'foxcatalog'
    password = 'foxcatalog'
    host = 'localhost'
    port = 3306
    databaseName = 'foxcatalog'

    def __init__(self):
        pass

    def connect(self):
        try:
            self.db = mariadb.connect(
                user=self.user,
                password=self.password,
                host=self.host,
                port=self.port,
                database=self.databaseName
            )
        except mariadb.Error as e:
            self.error = e

    def get_error(self):
        return self.error

