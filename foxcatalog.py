from flask import Flask, jsonify
from flask_cors import CORS

from FoxRepository import FoxRepository
from DbDriver import DbDriver

DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

dbDriver = DbDriver()
dbDriver.connect()

dbError = dbDriver.get_error()
if dbError:
    app.logger.error('Error connecting to MariaDB Platform: %s', dbError)

if DEBUG:
    CORS(app, resources={r'/*': {'origins': '*'}})

foxRepository = FoxRepository(db_driver=dbDriver)


@app.route('/api/v1/get-foxes', methods=['GET'])
def hello():
    data = foxRepository.get_foxes()

    response = {
        "message": "",
        "data": {
            "foxes": data,
        },
    }
    return jsonify(response)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
