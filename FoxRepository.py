import DbDriver


class FoxRepository:
    db_driver = None

    def __init__(self, db_driver):
        self.db_driver = db_driver

    def get_foxes(self):
        cursor = self.db_driver.db.cursor(dictionary=True)
        cursor.execute('SELECT * FROM `genera`;')
        data = []
        for (genus) in cursor:
            data.append(genus)
        return data
